package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || !statement.matches("^[\\d/*+\\-.()]*") || statement.length() < 2 ||
                statement.contains("++") || statement.contains("--") ||
                statement.contains("//") || statement.contains("**") || statement.contains("..")) {
            return null;
        }

        double resultDouble;

        try {
            resultDouble = brakesHandle(statement);
        } catch (Exception e) {
            return null;
        }

        if (Double.isInfinite(resultDouble)) {
            return null;
        }

        if (resultDouble % 1 == 0.0) {
            return String.valueOf((int) resultDouble);
        }

        Locale.setDefault(Locale.ENGLISH);
        DecimalFormat df = new DecimalFormat("#.####");
        return String.valueOf(df.format(resultDouble));
    }

    public double brakesHandle(String statement) {
        int j = statement.length() - 1;
        for (int i = 0; i < statement.length(); i++) {

            if (statement.charAt(i) == '(') {
                for (; j >= i; j--) {
                    if (statement.charAt(j) == ')') {
                        return splitter(statement.substring(0, i) +
                                brakesHandle(statement.substring(i + 1, j)) +
                                statement.substring(j + 1));
                    } else if (statement.charAt(j) == '(') {
                        throw new ArithmeticException();
                    }
                }
            } else if (statement.charAt(i) == ')') {
                throw new ArithmeticException();
            }
        }
        return splitter(statement);
    }

    public double splitter(String statement) {
        String[] arrayFor2Elements;
        double secondElement;

        try {
            if (statement.contains("+-")) {
                arrayFor2Elements = statement.split("\\+-", 2);
                return this.splitter(arrayFor2Elements[0]) - this.splitter(arrayFor2Elements[1]);
            }

            if (statement.contains("+")) {
                arrayFor2Elements = statement.split("\\+", 2);
                return this.splitter(arrayFor2Elements[0]) + this.splitter(arrayFor2Elements[1]);
            }

            if (statement.contains("--")) {
                arrayFor2Elements = statement.split("--", 2);
                return this.splitter(arrayFor2Elements[0]) + this.splitter(arrayFor2Elements[1]);
            }

            if (statement.contains("-") && !statement.contains("/-") && !statement.contains("*-")) {
                arrayFor2Elements = statement.split("-", 2);
                return this.splitter(arrayFor2Elements[0]) - this.splitter(arrayFor2Elements[1]);
            }

            if (statement.contains("*-")) {
                arrayFor2Elements = statement.split("\\*-", 2);
                secondElement = this.splitter(arrayFor2Elements[1]);
                return this.splitter(arrayFor2Elements[0]) * (-1.0) * secondElement;
            }

            if (statement.contains("*") && !statement.contains("*-")) {
                arrayFor2Elements = statement.split("\\*", 2);
                return this.splitter(arrayFor2Elements[0]) * this.splitter(arrayFor2Elements[1]);
            }

            if (statement.contains("/-")) {
                arrayFor2Elements = statement.split("/-", 2);
                secondElement = this.splitter(arrayFor2Elements[1]);
                return this.splitter(arrayFor2Elements[0]) * (-1.0) / secondElement;
            }

            if (statement.contains("/")) {
                arrayFor2Elements = statement.split("/", 2);
                return this.splitter(arrayFor2Elements[0]) / this.splitter(arrayFor2Elements[1]);
            }

            return Double.parseDouble(statement);

        } catch (Exception e) {
            throw new ArithmeticException();
        }
    }
}
