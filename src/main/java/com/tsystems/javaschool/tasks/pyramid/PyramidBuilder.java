package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        Integer previousElement = inputNumbers.get(1);

        for (Integer el : inputNumbers) {
            if (el == null) {
                throw new CannotBuildPyramidException();
            }

            if (previousElement.equals(el)) {
                throw new CannotBuildPyramidException();
            }
            previousElement = el;
        }

        inputNumbers.sort(Comparator.naturalOrder());

        int testQuantityElements = 1;
        int testQuantityElementsIterator = 2;
        int tableHight = 1;

        while (inputNumbers.size() > testQuantityElements) {
            testQuantityElements += testQuantityElementsIterator;
            ++testQuantityElementsIterator;
            ++tableHight;
        }

        if (inputNumbers.size() != testQuantityElements) {
            throw new CannotBuildPyramidException();
        }

        int tableWide = testQuantityElementsIterator * 2 - 3;
        int [][] resultTable = new int[tableHight][tableWide];

        int inputNumbersIterator = 0;
        for (int i = 0; i < tableHight; i++) {
            for (int j = 0; j < i + 1; j++) {
                resultTable[i][tableWide / 2 - i + j * 2] = inputNumbers.get(inputNumbersIterator);
                ++inputNumbersIterator;
            }
        }
        return resultTable;
    }
}
