package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        try {
            if (x.size() > y.size()) return false;
        } catch (Exception e) {
            return false;
        }

        int j = 0;
        for (Object o : x) {
            for (; j < y.size(); j++) {
                if (o.equals(y.get(j))) {
                    break;
                }
            }
            if (j >= y.size()) {
                return false;
            }
            ++j;
        }
        return true;
    }
}
